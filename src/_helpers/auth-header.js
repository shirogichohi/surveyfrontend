export function authHeader() {
  // return authorization header with jwt token
  let user = JSON.parse(localStorage.getItem("user"));

  if (user && user.key) {
    return { Authorization: "Token " + user.key };
  } else {
    return {};
  }
}
export function host() {
  return "http://192.168.60.24:8000/";
}
