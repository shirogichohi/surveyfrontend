import React, {Component} from "react";

export default class HandleError extends Component {

  render()
  {
    if (this.props.successMsg)
      return (
               <div className="alert alert-success">
                    <strong>Success!</strong> {this.props.successMsg.map(key => (key))}
                </div>)

        else if(this.props.errorMsg)
            return ( <div className="alert alert-danger">
                <strong>Error!</strong> {this.props.errorMsg.map(key => (key))}
            </div>)
        else
            return (<div></div>)

  }

  resetForm = () => {
   return (document.getElementById("form").reset())
  }
}
