import React, { Component } from "react";
import Tablemain from "../../components/Table";
import { connect } from "react-redux";
import axios from "axios";

export default class Officer extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentWillMount() {
    axios
      .get(`http://192.168.60.24:8000/api/officer/`)
      .then(res => {
        this.setState({ tablename: res.data });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    console.log("officer", this.state.tablename);
    return this.state.tablename && this.state.tablename.length ? (
      <Tablemain {...this.state} />
    ) : (
      <div>Loading the table.....</div>
    );
  }
}
