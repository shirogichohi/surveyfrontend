import React, { Component } from "react";
import Tablemain from "../../components/Table";
import axios from "axios";
import { authHeader } from '../_helpers/auth-header';

export default class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentWillMount() {
    const requestOptions = {
      method: 'GET',
      headers: authHeader()
    };
    axios
      .get(`http://192.168.60.24:8000/api/board`, { requestOptions })
      .then(res => {
        this.setState({ tablename: res.data });
      })
      .catch(function (error) {
        console.log(error);
      });
  }

  render() {
    return this.state.tablename && this.state.tablename.length ? (
      <Tablemain {...this.state} />
    ) : (
        <div>Loading the table.....</div>
      );
  }
}
