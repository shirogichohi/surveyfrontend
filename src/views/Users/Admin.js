import React, { Component } from "react";
import Tablemain from "../../components/Table";
import { connect } from "react-redux";
import axios from "axios";

export default class AdminUsers extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentWillMount() {
    axios
      .get(`http://192.168.60.24:8000/api/admin/`)
      .then(res => {
        this.setState({ tablename: res.data });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  render() {
    return this.state.tablename && this.state.tablename.length ? (
      <Tablemain {...this.state} />
    ) : (
      <div>Loading the table.....</div>
    );
  }
}
