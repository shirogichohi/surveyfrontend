import React, { Component } from 'react';
import axios from "axios";
import { authHeader } from '../../_helpers/auth-header'
/* Import Components */
import Input from "../../components/formelements//Input";
import Select from "../../components/formelements//Select";
import Button from "../../components/formelements/Button";
import Tablemain from "../../components/Table";
class Feedback extends Component {
    constructor() {
        super()

        this.state = {
            lattitude: '',
            longitude: '',
            load: true,
            switchResult: "",
            feedback: "",
            question: "",
            populate_questions: [],
            questionnaire: [],
            gender: [
                { id: "female", name: "female" },
                { id: "male", name: "male" },
            ],
            yesno: [
                { id: "yes", name: "yes" },
                { id: "no", name: "no" },
            ],
            questionnaire_id: "",
            feedbacks: []
        }

    }

    componentWillMount() {
        this.getMyLocation();
        axios
            .get(`http://192.168.60.24:8000/api/survey/`)
            .then(res => {
                let options = res.data.map(item => {
                    return { id: item.id, name: item.survey_description };
                });
                console.log("Options created -", { options });
                this.setState({ questionnaire: options });
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    getMyLocation = () => {
        const location = window.navigator && window.navigator.geolocation

        if (location) {
            location.getCurrentPosition((position) => {
                this.setState({
                    lattitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                })
            }, (error) => {
                this.setState({ lattitude: 'err-latitude', longitude: 'err-longitude', load: false })
            })
        }

    }


    getQuestions(survey_id) {
        var container = document.getElementById("questionDiv");
        let atr = { id: "question", title: "question", name: "question", value: this.state.question, placeholder: "Enter the question", handleChange: this.handleChange },
            gender_list = { id: "question", title: "question", name: "question", options: this.state.gender, value: this.state.selectedTeam, placeholder: "Select Type of gender", handleChange: this.handleChange },
            yesno = { id: "question", title: "question", name: "question", options: this.state.yesno, value: this.state.selectedTeam, placeholder: "Select Type of ..", handleChange: this.handleChange }
        let requestOptions = {
            method: 'GET',
            headers: authHeader(),
            body: JSON.stringify({ "survey_id": survey_id })
        };

        axios.get("http://192.168.60.24:8000/api/question/", requestOptions)
            .then(res => {
                this.setState({
                    data: res.data
                })
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    build_form = () => {
        let { data } = this.state
        if (data) {
            let atr = { id: "feedback", title: "feedback", name: "feedback", value: this.state.feedback, placeholder: "...", className: "form-control", handleChange: this.handleChange },
                gender_list = { id: "question", title: "question", name: "question", options: this.state.gender, value: this.state.selectedTeam, placeholder: "Select Type of gender", handleChange: this.handleChange },
                yesno = { id: "question", title: "question", name: "question", options: this.state.yesno, value: this.state.selectedTeam, placeholder: "Select Type of ..", handleChange: this.handleChange },
                { data } = this.state
            data = data.map(ques => {
                let elements = null,
                    placeholder = ques.question,
                    id = ques.id,
                    value = this.state.feedback ? this.state.feedback.find(item => item.id == id).feedback : null
                switch (ques.type) {
                    case "textfield":
                        elements = (
                            <>
                                <label>{ques.question}</label>
                                <input type='text' name='feedback' id={id} placeholder={placeholder} className="form-control" value={value} onChange={this.handleChange} />
                                <input type="hidden" name="questionnaire_id" id="questionnaire_id" value={id} />
                            </>
                        )
                        return elements
                        break;
                    case "datefield":
                        elements = (
                            <>
                                <label>{ques.question}</label>
                                <input type='date' name='feedback' id={id} placeholder={placeholder} className="form-control" value={value} onChange={this.handleChange} />
                                <input type="hidden" name="questionnaire_id" id="questionnaire_id" value={id} />
                            </>
                        )
                        return elements
                        break;
                    case "genderfield":

                        elements = (
                            <>
                                <label>{ques.question}</label>
                                <select name='feedback' id={id} placeholder={placeholder} className="form-control" value={value} onChange={this.handleChange}>
                                    {
                                        this.state.gender.forEach(function (gen) {
                                            return '<option value="' + gen.id + '">' + gen.name + '</option>';
                                        })
                                    }
                                </select>
                                <input type="hidden" name="questionnaire_id" id="questionnaire_id" value={id} />
                            </>
                        )
                        return elements
                        break;
                    case "integer":
                        elements = (
                            <>
                                <label>{ques.question}</label>
                                <input type='text' name='feedback' id={id} placeholder={placeholder} className="form-control" value={value} onChange={this.handleChange} />
                                <input type="hidden" name="questionnaire_id" id="questionnaire_id" value={id} />
                            </>
                        )
                        return elements
                        break;
                    case "yes/no":
                        elements = this.state.switchResult = (<Select {...yesno} />)
                        return elements
                        break;
                    default:
                        elements = this.state.switchResult = (<input {...atr} inputType="text" />)
                        return elements
                }
                return elements
            });
            return data
        } else {
            return (
                <div>
                    Getting questions .....
                </div>
            )
        }
    }

    listQuestions = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });

        this.getQuestions(this.state.questionnaire_id)

    }

    RECORDOLDHANDLECHANGE = (e) => {
        this.setState({
            ['feedbacks']: {
                'questionnaire_id': e.target.id,
                'feedback': e.target.value
            }
        });
        console.log('New state -', { state: this.state })

    }

    handleChange = (e) => {
        let feedbacks = this.state.feedbacks.filter(item => item.id !== e.target.id)
        this.setState({
            feedbacks: [
                ...feedbacks,
                { id: e.target.id, feedback: e.target.value }
            ]
        });
        console.log('New state -', { state: this.state })

    }

    handleFormSubmit = e => {
        e.preventDefault();
        const { longitude, lattitude, feedbacks } = this.state;
        feedbacks.map(res => {
            this.state.questionnaire_id = res.id
            this.state.feedback = res.feedback
            const { questionnaire_id, feedback } = this.state;
            axios
                .post("http://192.168.60.24:8000/api/answer/", { longitude, lattitude, questionnaire_id, feedback })
                .then(err => {
                    alert(err)
                });
        })

    };

    render() {
        const { lattitude, longitude } = this.state
        let lat_attr = { id: "lattitude", inputType: "text", title: "lattitude", name: "lattitude", value: this.state.lattitude, placeholder: "Enter the question", handleChange: this.handleChange },
            long_attr = { id: "longitude", inputType: "text", title: "longitude", name: "longitude", value: this.state.longitude, placeholder: "Enter the question", handleChange: this.handleChange },
            btn_attr = { action: this.handleFormSubmit, type: "primary", title: "Submit", style: buttonStyle },
            btn_clear = { action: this.handleClearForm, type: "secondary", title: "Clear", style: buttonStyle, },
            populate_questions = this.build_form(),
            userDetils = JSON.parse(localStorage.getItem('userDetails')),
            ques_options = this.state.questionnaire.map(item => {
                return <option value={item.id}>{item.name}</option>;
            })

        return (
            < div >
                {this.state.load == true ? (
                    <form className="container-fluid" onSubmit={this.handleFormSubmit} method="post" >
                        <div className="form-group">
                            <label for=""> Questionnaire </label>
                            <select className="form-control" onChange={this.listQuestions} name="questionnaire_id">
                                <option>--Select the questionnaire--</option>
                                {ques_options}
                            </select>
                        </div>
                        <div className="form-group">
                            <label for="" className="form-label">
                                Questions loading..................
                            </label>
                            <div id="questionDiv">
                                {populate_questions}
                            </div>
                        </div>

                        <Input {...long_attr} />
                        <Input {...lat_attr} />

                        <input
                            name="user_id"
                            className="form-control"
                            type="text"
                            value={userDetils.email}
                            onChange={this.handleChange}
                        />
                        <Button {...btn_attr} />
                        <Button {...btn_clear} />
                    </form>


                ) : (
                        <div><p>Location Has to be Set</p>
                            <button onClick={this.getMyLocation}>GET MY LOCATION</button>
                        </div>)}
            </div >
        )
    }
}
const buttonStyle = {
    margin: "10px 10px 10px 10px"
};
export default Feedback;