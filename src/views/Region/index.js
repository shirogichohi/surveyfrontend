import React, { Component } from "react";
import Tablemain from "../../components/Table";
import axios from "axios";

import Table from "react-bootstrap/Table";

class RegionView extends Component {
  state = {
    tablename: []
  };
  componentWillMount() {
    axios.get(`http://192.168.60.24:8000/api/region/`).then(res => {
      this.setState({ tablename: res.data });
    });
  }
  renderTableData() {
    return this.state.tablename.map((table, indext) => {
      return (
        <tr>
          {Object.keys(table).map((item, index) => {
            return <td>{table[item]}</td>;
          })}
        </tr>
      );
    });
  }

  render() {
    return (
      <div>
        <div>
          <div>
            <input type="text" placeholder="input something" ref="input" />
          </div>
          <div>
            <button type="button" onClick={this.openModal.bind(this)}>
              Open Modal{" "}
            </button>{" "}
          </div>
        </div>
        <Table striped bordered hover size="sm">
          <tbody>{this.renderTableData()}</tbody>
        </Table>
      </div>
    );
  }
}

export default RegionView;
