import React, { Component } from "react";
import axios from "axios";
import { Modal, ModalManager, Effect } from "react-dynamic-modal";
import MyModal from "../../components/Modal";
import Tablemain from "../../components/Table";
import Select from "../../components/Select";
class QuestionSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      items: []
    };
    axios.get("http://192.168.60.24:8000/api/survey").then(res => {
      res.data.map(results => {
        this.state.items.push({
          key: results.id,
          value: results.survey_description,
          field: "survey_id"
        });
      });
    });
  }
  componentWillMount() {
    var self = this;
    axios.get(`http://192.168.60.24:8000/api/survey/`).then(res => {
      this.setState({ tablename: res.data });
    });
  }

  handleSubmit = e => {
    e.preventDefault();
    const { survey_description, type, question } = this.state;
    axios
      .post("http://192.168.60.24:8000/api/question/", {
        survey_description,
        type,
        question
      })
      .then(result => { });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  openModal() {
    const text = (
      <div>
        <form
          method="POST"
          encType="multipart/form-data"
          className="form-horizontal"
          onSubmit={this.handleSubmit}
        >
          <fieldset>
            <div className="form-group ">
              <label className="col-sm-2 control-label ">Description</label>
              <div className="col-sm-10">
                <select
                  name="survey_id"
                  className="form-control"
                  onChange={this.handleChange}
                  value={this.state.value}
                >
                  {this.state.items.map(item => (
                    <option key={item.key} value={item.key}>
                      {item.value}
                    </option>
                  ))}
                </select>
              </div>
            </div>
            <div className="form-group ">
              <label className="col-sm-2 control-label ">
                Question encType
              </label>
              <div className="col-sm-10">
                <select
                  class="form-control"
                  name="type"
                  onChange={this.handleChange}
                  value={this.state.value}
                >
                  <option value="textfield">textfield</option>
                  <option value="genderfield">genderfield</option>
                  <option value="datefield">datefield</option>
                  <option value="integer">integer</option>
                  <option value="yes/no">yes/no</option>
                </select>
              </div>
            </div>
            <div className="form-group ">
              <label className="col-sm-2 control-label ">Question</label>
              <div className="col-sm-10">
                <input
                  name="question"
                  className="form-control"
                  type="text"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-actions">
              <button
                className="btn btn-primary js-tooltip"
                title="Make a POST request on the User Staff Viewset List resource"
              >
                POST
              </button>
            </div>
          </fieldset>
        </form>
      </div>
    );

    ModalManager.open(<MyModal text={text} onRequestClose={() => true} />);
  }

  render() {
    return (
      <div>
        <div className="mb-3 card card-body col-sm-2">
          <button
            type="button"
            className="btn btn-info"
            onClick={this.openModal.bind(this)}
          >
            Add new Record{" "}
          </button>{" "}
        </div>

        {this.state.tablename && this.state.tablename.length ? (
          <Tablemain {...this.state} />
        ) : (
            <div>Loading the table.....</div>
          )}
      </div>
    );
  }
}

export default QuestionSetting;
