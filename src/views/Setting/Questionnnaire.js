import React, { Component } from "react";
import axios from "axios";
import { Modal, ModalManager, Effect } from "react-dynamic-modal";
import MyModal from "../../components/Modal";
import Tablemain from "../../components/Table";
class QuestionnaireSetting extends Component {
  constructor(props) {
    super(props);
    this.state = {
      survey_description: ""
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    axios
      .post("http://192.168.60.24:8000/api/survey/", {
        ...this.state
      })
      .then(result => { });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  openModal() {
    const text = (
      <div>
        <form
          method="POST"
          encType="multipart/form-data"
          className="form-horizontal"
          onSubmit={this.handleSubmit}
        >
          <fieldset>
            <div className="form-group ">
              <label className="col-sm-2 control-label ">Description</label>
              <div className="col-sm-10">
                <input
                  name="survey_description"
                  className="form-control"
                  type="text"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-actions">
              <button
                className="btn btn-primary js-tooltip"
                title="Make a POST request on the User Staff Viewset List resource"
              >
                POST
              </button>
            </div>
          </fieldset>
        </form>
      </div>
    );

    ModalManager.open(<MyModal text={text} onRequestClose={() => true} />);
  }
  componentWillMount() {
    var self = this;
    axios.get(`http://192.168.60.24:8000/api/survey/`).then(res => {
      this.setState({ tablename: res.data });
    });
  }

  render() {
    return (
      <div>
        <div className="mb-3 card card-body col-sm-2">
          <button
            type="button"
            className="btn btn-info"
            onClick={this.openModal.bind(this)}
          >
            Add new Record{" "}
          </button>{" "}
        </div>

        {this.state.tablename && this.state.tablename.length ? (
          <Tablemain {...this.state} />
        ) : (
            <div>Loading the table.....</div>
          )}
      </div>
    );
  }
}

export default QuestionnaireSetting;
