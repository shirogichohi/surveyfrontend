import React, { Component } from "react";
import axios from "axios";
/* Import Components */
import Input from "../../components/formelements//Input";
import Select from "../../components/formelements//Select";
import Button from "../../components/formelements/Button";
import Tablemain from "../../components/Table";

class Question extends Component {
	state = {
		questionnaire: [],
		types: [
			{ id: "textfield", name: "textfield" },
			{ id: "datefield", name: "datefield" },
			{ id: "integer", name: "integer" },
			{ id: "yes/no", name: "yes/no" }
		],
		question: ""
	};

	constructor(props) {
		super(props);
		axios
			.get("http://192.168.60.24:8000/api/question/")
			.then(result => {
				this.setState({ tablename: result.data })
			});
	}

	componentWillMount() {
		axios
			.get(`http://192.168.60.24:8000/api/survey/`)
			.then(res => {
				let options = res.data.map(item => {
					return { id: item.id, name: item.survey_description };
				});
				console.log("Options created -", { options });
				this.setState({ questionnaire: options });
			})
			.catch(function (error) {
				console.log(error);
			});
	}

	handleChange = e => {
		this.setState({
			[e.target.name]: e.target.value
		});

		console.log('New state -', {
			[e.target.name]: e.target.value
		})
	};


	handleFormSubmit = e => {
		e.preventDefault();
		const { question, type, survey_id } = this.state;
		axios
			.post("http://192.168.60.24:8000/api/question/", { question, type, survey_id })
			.then(err => {
				//access the err here....
			});
	};


	render() {
		let input_attr = { id: "question", inputType: "text", title: "Question", name: "question", value: this.state.question, placeholder: "Enter the question", handleChange: this.handleChange },
			ques_attr = { id: "type", title: "Questionnaire", name: "type", options: this.state.types, value: this.state.selectedTeam, placeholder: "Select Type of Question", handleChange: this.handleChange },
			survey_attr = { id: "survey_id", title: "Questionnaire", name: "survey_id", options: this.state.questionnaire, value: this.state.survey_id, placeholder: "Select Questionnaire", handleChange: this.handleChange, },
			btn_attr = { action: this.handleFormSubmit, type: "primary", title: "Submit", style: buttonStyle },
			btn_clear = { action: this.handleClearForm, type: "secondary", title: "Clear", style: buttonStyle, }

		return (
			<div>
				<form className="container-fluid" onSubmit={this.handleFormSubmit} method="post" >
					<Input {...input_attr} />
					<Select {...ques_attr} />
					<Select {...survey_attr} />
					<Button {...btn_attr} />
					<Button {...btn_clear} />
				</form>
				{
					this.state.tablename && this.state.tablename.length ? (
						<Tablemain {...this.state} />
					) : (<div>Loading the table.....</div>)
				}

			</div >

		);
	}
}

const buttonStyle = {
	margin: "10px 10px 10px 10px"
};
export default Question;
