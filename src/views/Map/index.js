import React, { Component } from "react";
import axios from "axios";
/* Import Components */
import { Map, TileLayer, Marker, Popup, CircleMarker } from "react-leaflet";
import "leaflet/dist/leaflet.css";
import L from "leaflet";

import data from "./Cities";
class AdminHome extends Component {
  constructor() {
    super()

    this.state = {

      questionnaire: [],
      cities: []
    }
  }

  componentWillMount() {
    axios
      .get(`http://192.168.60.37:8000/api/survey/`)
      .then(res => {
        let options = res.data.map(item => {
          return { id: item.id, name: item.survey_description };
        });
        this.setState({ questionnaire: options });
      })
      .catch(function (error) {
        console.log(error);
      });
  }


  getLocations = (survey_id) => {
    axios
      .get("http://192.168.60.37:8000//api/answer/", { "survey_id": survey_id })
      .then(res => {
        console.log("Options created 1st-", res.data);
        let maps = res.data(item => {
          return { name: "Baringo", coordinates: [item.lattitude, item.longitude], population: 200 }
        })
        console.log("Options created22 -", maps);
        this.setState({
          cities: maps
        });
      })
      .catch(function (error) {
        console.log(error);
      });


  }
  listMarkers = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
    this.getLocations(3)
  }
  render() {
    delete L.Icon.Default.prototype._getIconUrl;

    L.Icon.Default.mergeOptions({
      iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
      iconUrl: require("leaflet/dist/images/marker-icon.png"),
      shadowUrl: require("leaflet/dist/images/marker-shadow.png")
    });
    let ques_options = this.state.questionnaire.map(item => {
      return <option value={item.id}>{item.name}</option>;
    })
    return (

      < div >
        {console.log("Robin sharma", this.state.cities)}
        <div className="form-group">
          <label for=""> Questionnaire </label>
          <select className="form-control" onChange={this.listMarkers} name="questionnaire_id">
            {ques_options}
          </select>
        </div>
        <div id="questionDiv">

        </div>
        <Map
          style={{ height: "1040px", width: "100%", margin: "2px" }}
          zoom={7}
          center={[0.0236, 37.9062]}
        >
          <TileLayer url="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png" />
          {this.state.cities ?
            data.city.map(city => {
              return (
                <Marker
                  position={[city["coordinates"][0], city["coordinates"][1]]}
                >
                  <Popup>
                    Total at {city.name} : {city.population}
                  </Popup>
                </Marker>
              );
            }) : data.city.map(city => {
              return (
                <Marker
                  position={[city["coordinates"][0], city["coordinates"][1]]}
                >
                  <Popup>
                    Total at {city.name} : {city.population}
                  </Popup>
                </Marker>
              );
            })
          }}
        </Map>
      </div >
    );
  }
}

export default AdminHome;
