import React, { Component } from 'react'
import { connect } from 'react-redux'
import { signIn, login } from '../../store/actions/authaction'
// import {Link} from "react-router-dom";
import { Redirect } from 'react-router-dom'

class Form extends Component {
  state = {
    username: null,
    password: null,
    username: null
  };

  handleOnChange = (e) => {
    this.setState({
      [e.target.id]: e.target.value
    });
  };

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.login(this.state)
  };


  render() {
    console.log("hang", this.props)
    return this.props.token ? <Redirect to='/map' /> : (
      <div className="col-sm-4 col-sm-offset-4">
        <div className="well">
          <form onSubmit={this.handleSubmit} method="POST" name="login_user_form">
            <div className="form-group row">
              <div className="col-md-4">
                <label htmlFor="email">Email Address</label>
              </div>
              <div className="col-md-8">
                <input className="form-control" id="email" name="email" required type="text" value={this.state.email || ''} onChange={this.handleOnChange} />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-md-4">
                <label htmlFor="username">Username</label>
              </div>
              <div className="col-md-8">
                <input className="form-control" id="username" name="username" required type="text" value={this.state.username || ''} onChange={this.handleOnChange} />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-md-4">
                <label htmlFor="password">Password</label>
              </div>
              <div className="col-md-8">
                <input className="form-control" id="password" name="password" required type="password" value={this.state.password || ''} onChange={this.handleOnChange} />
              </div>
            </div>
            <div className="form-group row">
              <div className="col-md-6 col-md-offset-3">
                <input className="btn btn-primary form-control" id="submit" name="submit" type="submit" value="Login" />
              </div>
            </div>
          </form>
        </div>
      </div>
    )
  }
}

const mapStateToProps = (state, props) => {
  return {
    ...state
  }

};

const mapDispatchToProps = (dispatch) => {
  return {
    login: (credentials) => dispatch(login(credentials)),
  }
};


export default connect(mapStateToProps, mapDispatchToProps)(Form)
