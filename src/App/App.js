import React from "react";
import { Router, Route } from "react-router-dom";
import { connect } from "react-redux";
import { Redirect } from "react-router";
import { history } from "../_helpers";
import { alertActions } from "../_actions";
import { PrivateRoute } from "../_components";
import { HomePage } from "../HomePage";
import { LoginPage } from "../LoginPage";
import ProfilePage from "../ProfilePage/ProfilePage";
import ChangePassword from "../ProfilePage/ChangePassword";
import Users from "../UsersPage/Users";
import UserDetailsPage from "../UserDetailsPage";
import Header from "../Layout/Header";
import Sidebar from "../Layout/Sidebar";
import Footer from "../Layout/Footer";
import QuestionnairePage from "../QuestionnairePage/";
import Question from "../QuestionPage/";
import Feedback from "../FeedbackPage/";
import MapHome from "../Map/";
import home from '../AdminHomePage/'
class App extends React.Component {
  constructor(props) {
    super(props);

    const { dispatch } = this.props;
    history.listen((location, action) => {
      // clear alert on location change
      dispatch(alertActions.clear());
    });
  }
  state = {
    loggedIn: false
  };

  render() {
    const { alert } = this.props;
    return (
      <div class="app-container app-theme-white body-tabs-shadow fixed-sidebar fixed-header">
        <Header />

        <div class="app-main">
          <Sidebar />
          <div class="app-main__outer">
            <div class="app-main__inner">
              {alert.message && (
                <div className={`alert ${alert.type}`}>{alert.message}</div>
              )}
              {console.log("logged oin ?", this.state)}
              <Router history={history}>
                <div>
                  <PrivateRoute exact path="/" component={HomePage} />
                   <PrivateRoute exact path="/home" component={home} />
                  <PrivateRoute path="/details" component={UserDetailsPage} />
                  <PrivateRoute path="/users" component={Users} />
                  <PrivateRoute path="/profile" component={ProfilePage} />
                  <PrivateRoute
                    path="/changepassword"
                    component={ChangePassword}
                  />
                  <PrivateRoute
                    path="/questionnaire"
                    component={QuestionnairePage}
                  />
                  <PrivateRoute path="/question" component={Question} />
                  <PrivateRoute path="/feedback" component={Feedback} />
                  <PrivateRoute path="/map" component={MapHome} />
                  <Route path="/login" component={LoginPage} />
                  <Route path="/logout" component={LoginPage} />
                </div>
              </Router>
            </div>

            <Footer />
          </div>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { alert } = state;
  return {
    alert
  };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App };
