import React, { Component } from "react";
import axios from "axios";
import Tablemain from "../components/Table";
import { authHeader, host } from "../_helpers/auth-header";
import HandleError  from "../_helpers/error-handler";
import {history} from "../_helpers";
class Questionnaire extends Component {
  constructor(props) {
    super(props);
    this.state = {
      survey_description: "",
      successMsg: "",
      errorMsg : ""
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    const { survey_description } = this.state;
    axios({
      method: "post",
      url: host() + "api/survey/",
      timeout: 4000,
      headers: authHeader(),
      data: { survey_description }
    }).then(res => {
                history.push('/questionnaire');
                this.setState({successMsg: "Record Added Successfully"});

            }, error => {
                let jsonner = error.response.data,
                    message = ''
                Object.keys(jsonner).map(item => {
                    message = message + item + ' : ' + jsonner[item] + ';\n'
                 })

                this.setState({errorMsg: [message]})
      }).catch(error => {
                   console.log(error)
                })
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  componentWillMount() {
    var self = this;
    axios({
      method: "get",
      url: host() + "api/survey/",
      timeout: 4000,
      headers: authHeader()
    }).then(res => {
      this.setState({ tablename: res.data });
    });
  }

  render() {
    return (
      <div>
          {this.state.successMsg || this.state.errorMsg ? (
              <HandleError {...this.state}/>
                 ) : (
          <div></div>
        )}
        <form
          method="POST"
          encType="multipart/form-data"
          className="form-horizontal"
          onSubmit={this.handleSubmit}
        >
          <fieldset>
            <div className="form-group ">
              <label className="col-sm-2 control-label ">Description</label>
              <div className="col-sm-10">
                <input
                  name="survey_description"
                  className="form-control"
                  type="text"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-group ">
              <div className="form-actions col-sm-10">
                <button
                  className="btn btn-primary js-tooltip"
                  title="Make a POST request on the User Staff Viewset List resource"
                >
                  SAVE
                </button>
              </div>
            </div>
          </fieldset>
        </form>

        {this.state.tablename && this.state.tablename.length ? (
          <Tablemain {...this.state} />
        ) : (
          <div>Loading the table.....</div>
        )}
      </div>
    );
  }
}

export default Questionnaire;
