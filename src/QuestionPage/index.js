import React, { Component } from "react";
import axios from "axios";
/* Import Components */
import Input from "../components/formelements//Input";
import Select from "../components/formelements//Select";
import Button from "../components/formelements/Button";
import Tablemain from "../components/Table";
import { authHeader, host } from "../_helpers/auth-header";
import {history} from "../_helpers";
import HandleError from "../_helpers/error-handler";
class Question extends Component {
  state = {
    questionnaire: [],
    types: [
      { id: "textfield", name: "textfield" },
      { id: "datefield", name: "datefield" },
      { id: "integer", name: "integer" },
      { id: "yes/no", name: "yes/no" }
    ],
    question: "",
    successMsg: "",
    errorMsg:""
  };

  constructor(props) {
    super(props);
    axios({
      method: "get",
      url: host() + "api/question/",
      timeout: 4000,
      headers: authHeader()
    }).then(result => {
      this.setState({ tablename: result.data });
    });
  }

  componentWillMount() {
    axios({
      method: "get",
      url: host() + "api/survey/",
      timeout: 4000,
      headers: authHeader()
    })
      .then(res => {
        let options = res.data.map(item => {
          return { id: item.id, name: item.survey_description };
        });
        this.setState({ questionnaire: options });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });

    console.log("New state -", {
      [e.target.name]: e.target.value
    });
  };

  handleFormSubmit = e => {
    e.preventDefault();
    const { question, type, survey_id } = this.state;
    axios({
      method: "post",
      url: host() + "api/question/",
      timeout: 4000,
      headers: authHeader(),
      data: {
        question,
        type,
        survey_id
      }
    }).then(res => {
         this.setState({successMsg: "Record Added Successfully"});
            history.push('/question')
            }, error => {
                let jsonner = error.response.data,
                    message = ''
                Object.keys(jsonner).map(item => {
                    message = message + item + ' : ' + jsonner[item] + ';\n'
                 })

                this.setState({errorMsg: [message]})
      }).catch(error => {
                   console.log(error)
                })

  };

  render() {
    let input_attr = {
        id: "question",
        inputType: "text",
        title: "Question",
        name: "question",
        value: this.state.question,
        placeholder: "Enter the question",
        handleChange: this.handleChange
      },
      ques_attr = {
        id: "type",
        title: "Type",
        name: "type",
        options: this.state.types,
        value: this.state.selectedTeam,
        placeholder: "Select Type of Question Type",
        handleChange: this.handleChange
      },
      survey_attr = {
        id: "survey_id",
        title: "Questionnaire",
        name: "survey_id",
        options: this.state.questionnaire,
        value: this.state.survey_id,
        placeholder: "Select Questionnaire",
        handleChange: this.handleChange
      },
      btn_attr = {
        action: this.handleFormSubmit,
        type: "primary",
        title: "Submit",
        style: buttonStyle
      },
      btn_clear = {
        action: this.handleClearForm,
        type: "secondary",
        title: "Clear",
        style: buttonStyle
      };

    return (
      <div>
         <HandleError {...this.state}/>
        <form
          className="container-fluid"
          onSubmit={this.handleFormSubmit}
          method="post"
        >
          <Select {...survey_attr} />
          <Select {...ques_attr} />
          <Input {...input_attr} />
          <Button {...btn_attr} />
          <Button {...btn_clear} />
        </form>
        {this.state.tablename && this.state.tablename.length ? (
          <Tablemain {...this.state} />
        ) : (
          <div>Loading the table.....</div>
        )}
      </div>
    );
  }
}

const buttonStyle = {
  margin: "10px 10px 10px 10px"
};
export default Question;
