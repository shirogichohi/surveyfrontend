

const data = {
  city: [
    {
      name: "Baringo",
      coordinates: [0.512912, 35.952537],
      population: 37843000
    },
    {
      name: "Bomet",
      coordinates: [-0.690131, 35.278005],
      population: 30539000
    },
    { name: "Embu", coordinates: [-0.5982, 37.653906], population: 24998000 },
    {
      name: "Garissa",
      coordinates: [-0.564679, 40.408457],
      population: 23480000
    },
    {
      name: "Kisumu",
      coordinates: [-0.167225, 34.953647],
      population: 23416000
    },
    {
      name: "Laikipia",
      coordinates: [0.311627, 36.814178],
      population: 22123000
    },
    {
      name: "Kirinyaga",
      coordinates: [-0.517766, 37.302006],
      population: 21009000
    },
    {
      name: "Turkana",
      coordinates: [3.318344, 35.401457],
      population: 17712000
    },
    { name: "Wajir", coordinates: [1.895541, 40.07933], population: 17444000 }
  ]
};

export default data;
