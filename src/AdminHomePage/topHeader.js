import React, { Component } from "react";
export default class TopHeader extends Component {
    render(){
        return(<div className="row">
    <div className="col-md-4">
        <div className="card mb-3 widget-content bg-midnight-bloom">
            <div className="widget-content-wrapper text-white">
                <div className="widget-content-left">
                    <div className="widget-heading">Total Questionnaires</div>
                    <div className="widget-subheading">Last year expenses</div>
                </div>
                <div className="widget-content-right">
                    <div className="widget-numbers text-white"><span>1896</span></div>
                </div>
            </div>
        </div>
    </div>
    <div className="col-md-4">
        <div className="card mb-3 widget-content bg-arielle-smile">
            <div className="widget-content-wrapper text-white">
                <div className="widget-content-left">
                    <div className="widget-heading">Total Responses</div>
                    <div className="widget-subheading">Total Clients Profit</div>
                </div>
                <div className="widget-content-right">
                    <div className="widget-numbers text-white"><span>$ 568</span></div>
                </div>
            </div>
        </div>
    </div>
    <div className="col-md-4">
        <div className="card mb-3 widget-content bg-grow-early">
            <div className="widget-content-wrapper text-white">
                <div className="widget-content-left">
                    <div className="widget-heading">Total Active Users</div>
                    <div className="widget-subheading">People Interested</div>
                </div>
                <div className="widget-content-right">
                    <div className="widget-numbers text-white"><span>46%</span></div>
                </div>
            </div>
        </div>
    </div>

</div>)
    }
}

