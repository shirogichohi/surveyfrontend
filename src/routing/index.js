import React, { Component } from "react";
import { BrowserRouter, Route, Link } from "react-router-dom";
import RegionView from "../../src/views/Region";
import Staff from "../views/Users/Staff";
import Board from "../views/Users/Board";
import AdminHome from "../views/Users/Admin";
import Officer from "../views/Users/Officer";
import Users from "../components/Users";
import Feedback from "../views/Feedback/";
import Map from "../views/Map";
import Login from "../views/Login";
import QuestionSetting from "../views/Setting/Questions";
import QuestionnaireSetting from "../views/Setting/Questionnnaire";
import Question from "../views/Setting/Question";
const routes = [
  {
    path: "/",
    component: Login
  },
  {
    path: "/map",
    component: Map
  },
  {
    path: "/users",
    component: Users
  },
  {
    path: "/region",
    component: RegionView
  },
  {
    path: "/staff",
    component: Staff
  },
  {
    path: "/board",
    component: Board
  },
  {
    path: "/officer",
    component: Officer
  },
  {
    path: "/adminusers",
    component: AdminHome
  },
  {
    path: "/questionnaire",
    component: QuestionnaireSetting
  },
  {
    path: "/questions",
    component: QuestionSetting
  },
  {
    path: "/feedback",
    component: Feedback
  }
];

class Routing extends Component {
  render() {
    return (
      <div>
        <BrowserRouter>
          <Route exact path="/" component={Login} />
          <Route exact path="/map" component={Map} />
          <Route path="/users" component={Users} />
          <Route path="/region" component={RegionView} />
          <Route path="/staff" component={Staff} />
          <Route path="/board" component={Board} />
          <Route path="/officer" component={Officer} />
          <Route path="/adminusers" component={AdminHome} />
          <Route path="/questionnaire" component={QuestionnaireSetting} />
          <Route path="/questions" component={QuestionSetting} />
          <Route path="/feedback" component={Feedback} />

        </BrowserRouter>
      </div>
    );
  }
}

export default Routing;
