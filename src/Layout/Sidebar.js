import React, { Component } from "react";
import ReactDOM from "react-dom";
export default class Sidebar extends Component {
  state = {};

  render() {
    let userDetils = JSON.parse(localStorage.getItem("userDetails"));
    let questionnaire_id = JSON.parse(localStorage.getItem("questionnaire_id"));
    if (userDetils){
      //admin
      if(userDetils.role == "admin"){
        return (     <div className="app-sidebar sidebar-shadow bg-asteroid sidebar-text-light">
        <div className="app-header__logo">
          <div className="logo-src" />
          <div className="header__pane ml-auto">
            <div>
              <button
                type="button"
                className="hamburger close-sidebar-btn hamburger--elastic"
                data-class="closed-sidebar"
              >
                <span className="hamburger-box">
                  <span className="hamburger-inner" />
                </span>
              </button>
            </div>
          </div>
        </div>
        <div className="app-header__mobile-menu">
          <div>
            <button
              type="button"
              className="hamburger hamburger--elastic mobile-toggle-nav"
            >
              <span className="hamburger-box">
                <span className="hamburger-inner" />
              </span>
            </button>
          </div>
        </div>
        <div className="app-header__menu">
          <span>
            <button
              type="button"
              className="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav"
            >
              <span className="btn-icon-wrapper">
                <i className="fa fa-ellipsis-v fa-w-6" />
              </span>
            </button>
          </span>
        </div>
        <div className="scrollbar-sidebar">
          <div className="app-sidebar__inner">
            <ul className="vertical-nav-menu">
              {userDetils.role == "admin" ? (
                <div>
                  <li className="app-sidebar__heading">Dashboards</li>
                  <li>
                    <a href="/home" className="mm-active">
                      <i className="metismenu-icon pe-7s-rocket" />
                      Dashboard
                    </a>
                  </li>
                  <li className="app-sidebar__heading">User Management</li>
                  <li>
                    <a href="/users" className="mm-active">
                      <i className="metismenu-icon pe-7s-rocket" />
                      Users
                    </a>
                  </li>
                  <li className="app-sidebar__heading">User Management</li>
                  <li>
                    <a href="/staff">
                      <i className="metismenu-icon pe-7s-diamond" />
                      Staffs
                      <i className="metismenu-state-icon pe-7s-angle-down caret-left" />
                    </a>
                    <ul>
                      <li>
                        <a href="/staff">
                          <i className="metismenu-icon"></i>
                          Details
                        </a>
                      </li>
                      <li>
                        <a href="/">
                          <i className="metismenu-icon" />
                          System Logs
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="#">
                      <i className="metismenu-icon pe-7s-car" />
                      Officers
                      <i className="metismenu-state-icon pe-7s-angle-down caret-left" />
                    </a>
                    <ul>
                      <li>
                        <a href="/officer">
                          <i className="metismenu-icon" />
                          System Logs
                        </a>
                      </li>
                      <li>
                        <a href="/staff">
                          <i className="metismenu-icon"></i>
                          Dashboard
                        </a>
                      </li>
                      <li>
                        <a href="/staff">
                          <i className="metismenu-icon"></i>
                          Details
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="/board">
                      <i className="metismenu-icon pe-7s-display2" />
                      Board
                    </a>
                  </li>
                  <li className="app-sidebar__heading">Analysis</li>
                  <li>
                    <a href="/map">
                      <i className="metismenu-icon pe-7s-display2" />
                      Overall Analysis
                    </a>
                  </li>

                  <li className="app-sidebar__heading">Questionnaire</li>
                  <li>
                    <a href="questionnaire">
                      <i className="metismenu-icon pe-7s-mouse"></i>
                      Questionnaire Category
                    </a>
                  </li>
                  <li>
                    <a href="question">
                      <i className="metismenu-icon pe-7s-eyedropper"></i>
                      Questions
                    </a>
                  </li>
                  <li>
                    <a href="feedback">
                      <i className="metismenu-icon pe-7s-pendrive"></i>Feedback
                      Analysis
                    </a>
                  </li>
                  <li className="app-sidebar__heading">Charts</li>
                  <li>
                    <a href="charts-chartjs.html">
                      <i className="metismenu-icon pe-7s-graph2"></i>Regional
                      Representation
                    </a>
                  </li>
                  <li>
                    <a href="charts-chartjs.html">
                      <i className="metismenu-icon pe-7s-graph2"></i>Country
                      Representation
                    </a>
                  </li>
                  <li className="app-sidebar__heading">REPORTS</li>
                </div>
              ) : (
                <div></div>
              )}
              {userDetils.role == "board" ? (
                <div>
                  <li className="app-sidebar__heading">BOARD PANEL</li>
                  <li>
                    <a href="/map" className="mm-active">
                      <i className="metismenu-icon pe-7s-rocket" />
                      Dashboard
                    </a>
                  </li>
                  <li>
                    <a href="/feedback">
                      <i className="metismenu-icon pe-7s-eyedropper"></i> Take
                      Survey
                    </a>
                  </li>
                </div>
              ) : (
                <div></div>
              )}
              {userDetils.role == "staff" ? (
                <div>
                  <li className="app-sidebar__heading">STAFF PANEL</li>
                  <li>
                    <a href="/map" className="mm-active">
                      <i className="metismenu-icon pe-7s-rocket" />
                      Dashboard
                    </a>
                  </li>
                  <li>
                    <a href="/feedback">
                      <i className="metismenu-icon pe-7s-eyedropper"></i> Take
                      Survey
                    </a>
                  </li>
                </div>
              ) : (
                <div></div>
              )}
              {userDetils.role == "officer" ? (
                <div>
                  <li className="app-sidebar__heading">
                    REGIONAL OFFICER PANEL
                  </li>
                  <li>
                    <a href="/map" className="mm-active">
                      <i className="metismenu-icon pe-7s-rocket" />
                      Dashboard
                    </a>
                  </li>
                  <li>
                    <a href="/feedback">
                      <i className="metismenu-icon pe-7s-eyedropper"></i> Users
                    </a>
                  </li>
                </div>
              ) : (
                <div></div>
              )}
            </ul>
          </div>
        </div>
      </div>
 )
      }
    } else if(userDetils && questionnaire_id){
      return (<div className="app-sidebar sidebar-shadow bg-asteroid sidebar-text-light">
        <div className="app-header__logo">
          <div className="logo-src" />
          <div className="header__pane ml-auto">
            <div>
              <button
                type="button"
                className="hamburger close-sidebar-btn hamburger--elastic"
                data-class="closed-sidebar"
              >
                <span className="hamburger-box">
                  <span className="hamburger-inner" />
                </span>
              </button>
            </div>
          </div>
        </div>
        <div className="app-header__mobile-menu">
          <div>
            <button
              type="button"
              className="hamburger hamburger--elastic mobile-toggle-nav"
            >
              <span className="hamburger-box">
                <span className="hamburger-inner" />
              </span>
            </button>
          </div>
        </div>
        <div className="app-header__menu">
          <span>
            <button
              type="button"
              className="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav"
            >
              <span className="btn-icon-wrapper">
                <i className="fa fa-ellipsis-v fa-w-6" />
              </span>
            </button>
          </span>
        </div>
        <div className="scrollbar-sidebar">
          <div className="app-sidebar__inner">
            <ul className="vertical-nav-menu">
              {userDetils.role == "admin" ? (
                <div>
                  <li className="app-sidebar__heading">Dashboards</li>
                  <li>
                    <a href="/" className="mm-active">
                      <i className="metismenu-icon pe-7s-rocket" />
                      Dashboard
                    </a>
                  </li>
                  <li className="app-sidebar__heading">User Management</li>
                  <li>
                    <a href="/users" className="mm-active">
                      <i className="metismenu-icon pe-7s-rocket" />
                      Users
                    </a>
                  </li>
                  <li className="app-sidebar__heading">User Management</li>
                  <li>
                    <a href="/staff">
                      <i className="metismenu-icon pe-7s-diamond" />
                      Staffs
                      <i className="metismenu-state-icon pe-7s-angle-down caret-left" />
                    </a>
                    <ul>
                      <li>
                        <a href="/staff">
                          <i className="metismenu-icon"></i>
                          Details
                        </a>
                      </li>
                      <li>
                        <a href="/">
                          <i className="metismenu-icon" />
                          System Logs
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="#">
                      <i className="metismenu-icon pe-7s-car" />
                      Officers
                      <i className="metismenu-state-icon pe-7s-angle-down caret-left" />
                    </a>
                    <ul>
                      <li>
                        <a href="/officer">
                          <i className="metismenu-icon" />
                          System Logs
                        </a>
                      </li>
                      <li>
                        <a href="/staff">
                          <i className="metismenu-icon"></i>
                          Dashboard
                        </a>
                      </li>
                      <li>
                        <a href="/staff">
                          <i className="metismenu-icon"></i>
                          Details
                        </a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="/board">
                      <i className="metismenu-icon pe-7s-display2" />
                      Board
                    </a>
                  </li>
                  <li className="app-sidebar__heading">Analysis</li>
                  <li>
                    <a href="/map">
                      <i className="metismenu-icon pe-7s-display2" />
                      Overall Analysis
                    </a>
                  </li>

                  <li className="app-sidebar__heading">Questionnaire</li>
                  <li>
                    <a href="questionnaire">
                      <i className="metismenu-icon pe-7s-mouse"></i>
                      Questionnaire Category
                    </a>
                  </li>
                  <li>
                    <a href="question">
                      <i className="metismenu-icon pe-7s-eyedropper"></i>
                      Questions
                    </a>
                  </li>
                  <li>
                    <a href="feedback">
                      <i className="metismenu-icon pe-7s-pendrive"></i>Feedback
                      Analysis
                    </a>
                  </li>
                  <li className="app-sidebar__heading">Charts</li>
                  <li>
                    <a href="charts-chartjs.html">
                      <i className="metismenu-icon pe-7s-graph2"></i>Regional
                      Representation
                    </a>
                  </li>
                  <li>
                    <a href="charts-chartjs.html">
                      <i className="metismenu-icon pe-7s-graph2"></i>Country
                      Representation
                    </a>
                  </li>
                  <li className="app-sidebar__heading">REPORTS</li>
                </div>
              ) : (
                <div></div>
              )}
              {userDetils.role == "board" ? (
                <div>
                  <li className="app-sidebar__heading">BOARD PANEL</li>
                  <li>
                    <a href="/map" className="mm-active">
                      <i className="metismenu-icon pe-7s-rocket" />
                      Dashboard
                    </a>
                  </li>
                  <li>
                    <a href="/feedback">
                      <i className="metismenu-icon pe-7s-eyedropper"></i> Take
                      Survey
                    </a>
                  </li>
                </div>
              ) : (
                <div></div>
              )}
              {userDetils.role == "staff" ? (
                <div>
                  <li className="app-sidebar__heading">STAFF PANEL</li>
                  <li>
                    <a href="/map" className="mm-active">
                      <i className="metismenu-icon pe-7s-rocket" />
                      Dashboard
                    </a>
                  </li>
                  <li>
                    <a href="/feedback">
                      <i className="metismenu-icon pe-7s-eyedropper"></i> Take
                      Survey
                    </a>
                  </li>
                </div>
              ) : (
                <div></div>
              )}
              {userDetils.role == "officer" ? (
                <div>
                  <li className="app-sidebar__heading">
                    REGIONAL OFFICER PANEL
                  </li>
                  <li>
                    <a href="/map" className="mm-active">
                      <i className="metismenu-icon pe-7s-rocket" />
                      Dashboard
                    </a>
                  </li>
                  <li>
                    <a href="/feedback">
                      <i className="metismenu-icon pe-7s-eyedropper"></i> Users
                    </a>
                  </li>
                </div>
              ) : (
                <div></div>
              )}
            </ul>
          </div>
        </div>
      </div>
 )

    } else {
      return (<div style={{ backgroundColor: "#0c0b0f", height:"200px"}}>></div>)
    }

  }
}
