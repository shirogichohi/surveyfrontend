import React, { Component } from "react";
import ReactDOM from "react-dom";
export default class Header extends Component {
  state = {};
  render() {
    const userDetils = JSON.parse(localStorage.getItem("userDetails"));
    const questionnaire_id = JSON.parse(
      localStorage.getItem("questionnaire_id")
    );
     if (userDetils){
       if(userDetils.role == "admin"){
         return (<div className="app-header header-shadow bg-alternate header-text-light">
               <div className="app-header__logo">
                 <div className="logo-srcc"/>
                 <div className="header__pane ml-auto">
                   <div>
                     <button
                         type="button"
                         className="hamburger close-sidebar-btn hamburger--elastic"
                         data-class="closed-sidebar"
                     >
                <span className="hamburger-box">
                  <span className="hamburger-inner"/>
                </span>
                     </button>
                   </div>
                 </div>
               </div>
               <div className="app-header__mobile-menu">
                 <div>
                   <button
                       type="button"
                       className="hamburger hamburger--elastic mobile-toggle-nav"
                   >
              <span className="hamburger-box">
                <span className="hamburger-inner"/>
              </span>
                   </button>
                 </div>
               </div>
               <div className="app-header__menu">
          <span>
            <button
                type="button"
                className="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav"
            >
              <span className="btn-icon-wrapper">
                <i className="fa fa-ellipsis-v fa-w-6"/>
              </span>
            </button>
          </span>
               </div>
               <div className="app-header__content">
                 <div className="app-header-left">
                   <div className="search-wrapper">
                     <div className="input-holder">
                       <input
                           type="text"
                           className="search-input"
                           placeholder="Type to search"
                       />
                       <button className="search-icon">
                         <span/>
                       </button>
                     </div>
                     <button className="close"/>
                   </div>
                   <ul className="header-menu nav">
                     <li className="nav-item">
                       <a href="javascript:void(0);" className="nav-link">
                         <i className="nav-link-icon fa fa-database"> </i>
                         Statistics
                       </a>
                     </li>
                     <li className="btn-group nav-item">
                       <a href="javascript:void(0);" className="nav-link">
                         <i className="nav-link-icon fa fa-edit"/>
                         Projects
                       </a>
                     </li>
                     <li className="dropdown nav-item">
                       <a href="javascript:void(0);" className="nav-link">
                         <i className="nav-link-icon fa fa-cog"/>
                         Settings
                       </a>
                     </li>
                   </ul>
                 </div>
                 <div className="app-header-right">
                   <div className="header-btn-lg pr-0">
                     <div className="widget-content p-0">
                       <div className="widget-content-wrapper">
                         <div className="widget-content-left">
                           <div className="btn-group">
                             <a
                                 data-toggle="dropdown"
                                 aria-haspopup="true"
                                 aria-expanded="false"
                                 className="p-0 btn"
                             >
                               <img
                                   className="rounded-circle"
                                   src="assets/images/avatars/1.jpg"
                                   alt=""
                                   width={42}
                               />
                               <i className="fa fa-angle-down ml-2 opacity-8"/>
                             </a>
                             <div
                                 tabIndex={-1}
                                 role="menu"
                                 aria-hidden="true"
                                 className="dropdown-menu dropdown-menu-right"
                             >
                               <a
                                   className="button"
                                   className="dropdown-item"
                                   href="profile"
                               >
                                 User Account
                               </a>

                               <a
                                   className="button"
                                   className="dropdown-item"
                                   href="changepassword"
                               >
                                 Change Password
                               </a>

                               <div tabIndex={-1} className="dropdown-divider"/>
                               <a
                                   className="button"
                                   className="dropdown-item"
                                   href="login"
                               >
                                 Logout
                               </a>
                             </div>
                           </div>
                         </div>
                         <div className="widget-content-left  ml-3 header-user-info">
                           <div className="widget-heading">{userDetils.email}</div>
                           <div className="widget-subheading">{userDetils.role}</div>
                         </div>
                         <div className="widget-content-right header-user-info ml-3">
                           <button
                               type="button"
                               className="btn-shadow p-1 btn btn-primary btn-sm show-toastr-example"
                           >
                             <i className="fa text-white fa-calendar pr-1 pl-1"/>
                           </button>
                         </div>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
         )
       }
     } else if (userDetils && questionnaire_id) {
       return (<div className="app-header header-shadow bg-alternate header-text-light">
             <div className="app-header__logo">
               <div className="logo-srcc"/>
               <div className="header__pane ml-auto">
                 <div>
                   <button
                       type="button"
                       className="hamburger close-sidebar-btn hamburger--elastic"
                       data-class="closed-sidebar"
                   >
                <span className="hamburger-box">
                  <span className="hamburger-inner"/>
                </span>
                   </button>
                 </div>
               </div>
             </div>
             <div className="app-header__mobile-menu">
               <div>
                 <button
                     type="button"
                     className="hamburger hamburger--elastic mobile-toggle-nav"
                 >
              <span className="hamburger-box">
                <span className="hamburger-inner"/>
              </span>
                 </button>
               </div>
             </div>
             <div className="app-header__menu">
          <span>
            <button
                type="button"
                className="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav"
            >
              <span className="btn-icon-wrapper">
                <i className="fa fa-ellipsis-v fa-w-6"/>
              </span>
            </button>
          </span>
             </div>
             <div className="app-header__content">
               <div className="app-header-left">
                 <div className="search-wrapper">
                   <div className="input-holder">
                     <input
                         type="text"
                         className="search-input"
                         placeholder="Type to search"
                     />
                     <button className="search-icon">
                       <span/>
                     </button>
                   </div>
                   <button className="close"/>
                 </div>
                 <ul className="header-menu nav">
                   <li className="nav-item">
                     <a href="javascript:void(0);" className="nav-link">
                       <i className="nav-link-icon fa fa-database"> </i>
                       Statistics
                     </a>
                   </li>
                   <li className="btn-group nav-item">
                     <a href="javascript:void(0);" className="nav-link">
                       <i className="nav-link-icon fa fa-edit"/>
                       Projects
                     </a>
                   </li>
                   <li className="dropdown nav-item">
                     <a href="javascript:void(0);" className="nav-link">
                       <i className="nav-link-icon fa fa-cog"/>
                       Settings
                     </a>
                   </li>
                 </ul>
               </div>
               <div className="app-header-right">
                 <div className="header-btn-lg pr-0">
                   <div className="widget-content p-0">
                     <div className="widget-content-wrapper">
                       <div className="widget-content-left">
                         <div className="btn-group">
                           <a
                               data-toggle="dropdown"
                               aria-haspopup="true"
                               aria-expanded="false"
                               className="p-0 btn"
                           >
                             <img
                                 className="rounded-circle"
                                 src="assets/images/avatars/1.jpg"
                                 alt=""
                                 width={42}
                             />
                             <i className="fa fa-angle-down ml-2 opacity-8"/>
                           </a>
                           <div
                               tabIndex={-1}
                               role="menu"
                               aria-hidden="true"
                               className="dropdown-menu dropdown-menu-right"
                           >
                             <a
                                 className="button"
                                 className="dropdown-item"
                                 href="profile"
                             >
                               User Account
                             </a>

                             <a
                                 className="button"
                                 className="dropdown-item"
                                 href="changepassword"
                             >
                               Change Password
                             </a>

                             <div tabIndex={-1} className="dropdown-divider"/>
                             <a
                                 className="button"
                                 className="dropdown-item"
                                 href="login"
                             >
                               Logout
                             </a>
                           </div>
                         </div>
                       </div>
                       <div className="widget-content-left  ml-3 header-user-info">
                         <div className="widget-heading">{userDetils.email}</div>
                         <div className="widget-subheading">{userDetils.role}</div>
                       </div>
                       <div className="widget-content-right header-user-info ml-3">
                         <button
                             type="button"
                             className="btn-shadow p-1 btn btn-primary btn-sm show-toastr-example"
                         >
                           <i className="fa text-white fa-calendar pr-1 pl-1"/>
                         </button>
                       </div>
                     </div>
                   </div>
                 </div>
               </div>
             </div>
           </div>
       )
     } else {
  return (<div></div>)
     }

  }
}
