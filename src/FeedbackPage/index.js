import React, { Component } from "react";
import axios from "axios";
/* Import Components */
import Input from "../components/formelements//Input";
import Select from "../components/formelements//Select";
import Button from "../components/formelements/Button";
import { authHeader, host } from "../_helpers/auth-header";
import {history} from "../_helpers";
class Feedback extends Component {
  constructor() {
    super();

    this.state = {
      lattitude: "",
      longitude: "",
      load: true,
      switchResult: "",
      feedback: "",
      question: "",
      populate_questions: [],
      questionnaire: [],
      gender: [{ id: "female", name: "female" }, { id: "male", name: "male" }],
      yesno: [{ id: "yes", name: "yes" }, { id: "no", name: "no" }],
      questionnaire_id: "",
      feedbacks: [],
      successMsg: "",
        userEmail : "",
        survey_id : ""
    };
    this.getQuestions(1);
  }

  componentWillMount() {
    this.getMyLocation();
  }
  getMyLocation = () => {
    const location = window.navigator && window.navigator.geolocation;

    if (location) {
      location.getCurrentPosition(
        position => {
          this.setState({
            lattitude: position.coords.latitude,
            longitude: position.coords.longitude
          });
          alert("Getting Location.");
        },
        error => {
          this.setState({
            lattitude: "37.9943",
            longitude: "2.3355",
            load: false
          });
          alert("Sorry, your browser does not support HTML5 geolocation.");
        }
      );
    }
  };

  getQuestions(survey_id) {
    var container = document.getElementById("questionDiv");
    let atr = {
        id: "question",
        title: "question",
        name: "question",
        value: this.state.question,
        placeholder: "Enter the question",
        handleChange: this.handleChange
      },
      gender_list = {
        id: "question",
        title: "question",
        name: "question",
        options: this.state.gender,
        value: this.state.selectedTeam,
        placeholder: "Select Type of gender",
        handleChange: this.handleChange
      },
      yesno = {
        id: "question",
        title: "question",
        name: "question",
        options: this.state.yesno,
        value: this.state.selectedTeam,
        placeholder: "Select Type of ..",
        handleChange: this.handleChange
      };

    axios({
      method: "get",
      url: host() + "api/question/?survey_id="+survey_id,
      timeout: 4000,
      headers: authHeader()
    })
      .then(res => {
        this.setState({
          data: res.data
        });
      })
      .catch(function(error) {
        console.log(error);
      });
  }

  build_form = () => {
    let { data } = this.state;
    if (data) {
      let atr = {
          id: "feedback",
          title: "feedback",
          name: "feedback",
          value: this.state.feedback,
          placeholder: "...",
          className: "form-control",
          handleChange: this.handleChange
        },
        gender_list = {
          id: "question",
          title: "question",
          name: "question",
          options: this.state.gender,
          value: this.state.selectedTeam,
          placeholder: "Select Type of gender",
          handleChange: this.handleChange
        },
        yesno = {
          id: "question",
          title: "question",
          name: "question",
          options: this.state.yesno,
          value: this.state.selectedTeam,
          placeholder: "Select Type of ..",
          handleChange: this.handleChange
        },
        { data } = this.state;
          data = data.map(ques => {
              let elements = null,
                  placeholder = ques.question,
                  id = ques.id,
                  value = this.state.feedback
                      ? this.state.feedback.find(item => item.id == id).feedback
                      : null;
              switch (ques.type) {
                  case "textfield":
                      elements = (
                          <>
                              <label>{ques.question}</label>
                              <input
                                  type="text"
                                  name="feedback"
                                  id={id}
                                  placeholder={placeholder}
                                  className="form-control"
                                  value={value}
                                  onChange={this.handleChange}
                              />
                              <input
                                  type="hidden"
                                  name="questionnaire_id"
                                  id="questionnaire_id"
                                  value={id}
                              />
                          </>
                      );
                      return elements;
                      break;
                  case "datefield":
                      elements = (
                          <>
                              <label>{ques.question}</label>
                              <input
                                  type="date"
                                  name="feedback"
                                  id={id}
                                  placeholder={placeholder}
                                  className="form-control"
                                  value={value}
                                  onChange={this.handleChange}
                              />
                              <input
                                  type="hidden"
                                  name="questionnaire_id"
                                  id="questionnaire_id"
                                  value={id}
                              />
                          </>
                      );
                      return elements;
                      break;
                  case "genderfield":
                      elements = (
                          <>
                              <label>{ques.question}</label>
                              <select
                                  name="feedback"
                                  id={id}
                                  placeholder={placeholder}
                                  className="form-control"
                                  value={value}
                                  onChange={this.handleChange}
                              >
                                  {this.state.gender.forEach(function (gen) {
                                      return (
                                          '<option value="' + gen.id + '">' + gen.name + "</option>"
                                      );
                                  })}
                              </select>
                              <input
                                  type="hidden"
                                  name="questionnaire_id"
                                  id="questionnaire_id"
                                  value={id}
                              />
                          </>
                      );
                      return elements;
                      break;
                  case "integer":
                      elements = (
                          <>
                              <label>{ques.question}</label>
                              <input
                                  type="text"
                                  name="feedback"
                                  id={id}
                                  placeholder={placeholder}
                                  className="form-control"
                                  value={value}
                                  onChange={this.handleChange}
                              />
                              <input
                                  type="hidden"
                                  name="questionnaire_id"
                                  id="questionnaire_id"
                                  value={id}
                              />
                          </>
                      );
                      return elements;
                      break;
                  case "yes/no":
                      elements = this.state.switchResult = <Select {...yesno} />;
                      return elements;
                      break;
                  default:
                      elements = this.state.switchResult = (
                          <input {...atr} inputType="text"/>
                      );
                      return elements;
              }
              return elements;
          });

      return data;
    } else {
      return <div>Getting questions .....</div>;
    }
  };


  RECORDOLDHANDLECHANGE = e => {
    this.setState({
      ["feedbacks"]: {
        questionnaire_id: e.target.id,
        feedback: e.target.value
      }
    });
    console.log("New state -", { state: this.state });
  };

  handleChange = e => {
    let feedbacks = this.state.feedbacks.filter(
      item => item.id !== e.target.id
    );
    this.setState({
      feedbacks: [...feedbacks, { id: e.target.id, feedback: e.target.value }]
    });
  };

  handleFormSubmit = e => {
    e.preventDefault();

    let user = localStorage.getItem('userDetails'),
        userEmail = user.email,
        survey_id = localStorage.getItem('questionnaire_id')
     const { longitude, lattitude, feedbacks  } = this.state;
    feedbacks.map(res => {
      this.state.questionnaire_id = res.id;
      this.state.feedback = res.feedback;
      const { questionnaire_id, feedback } = this.state;
      axios({
        method: "post",
        url: host() + "api/answer/",
        timeout: 4000,
        headers: authHeader(),
        data: {
          longitude,
          lattitude,
          questionnaire_id,
          feedback
        }
      }).then(res => {
             this.setState({successMsg: "Record Added Successfully"});
            history.push('/map');
            }, error => {
                let jsonner = error.response.data,
                    message = ''
                Object.keys(jsonner).map(item => {
                    message = message + item + ' : ' + jsonner[item] + ';\n'
                 })

                this.setState({errorMsg: [message]})
      }).catch(error => {
                   console.log(error)
                })
    });
       history.push('/map');

  };

  render() {
    const { lattitude, longitude } = this.state;
    let lat_attr = {
        id: "lattitude",
        inputType: "text",
        title: "lattitude",
        name: "lattitude",
        value: this.state.lattitude,
        placeholder: "Enter the question",
        handleChange: this.handleChange
      },
      long_attr = {
        id: "longitude",
        inputType: "text",
        title: "longitude",
        name: "longitude",
        value: this.state.longitude,
        placeholder: "Enter the question",
        handleChange: this.handleChange
      },
      btn_attr = {
        action: this.handleFormSubmit,
        type: "primary",
        title: "Submit",
        style: buttonStyle
      },
      btn_clear = {
        action: this.handleClearForm,
        type: "secondary",
        title: "Clear",
        style: buttonStyle
      },
      populate_questions = this.build_form()

    return (
      <div>
        {this.state.load == true ? (
          <form
            className="container-fluid"
            onSubmit={this.handleFormSubmit}
            method="post"
          >
            <div className="form-group">
              <label for="" className="form-label">
                <strong> List of Questions</strong>
              </label>
              <div id="questionDiv">{populate_questions}</div>
            </div>

            <Input {...long_attr} />
            <Input {...lat_attr} />

            <Button {...btn_attr} />
            <Button {...btn_clear} />
          </form>
        ) : (
          <div>
            <p>Location Has to be Set</p>
              <button type="button" onClick="getMyLocation();">Show Position</button>

          </div>
        )}
      </div>
    );
  }
}
const buttonStyle = {
  margin: "10px 10px 10px 10px"
};
export default Feedback;
