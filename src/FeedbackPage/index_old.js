import React, { Component } from 'react';
import axios from "axios";
/* Import Components */
import Input from "../../components/formelements//Input";
import Select from "../../components/formelements//Select";
import Button from "../../components/formelements/Button";
import Tablemain from "../../components/Table";
class Feedback extends Component {
    constructor() {
        super()

        this.state = {
            lattitude: '',
            longitude: '',
            load: true,
            switchResult: "",
            feedback: "",
            questions: [],
            questionnaire: [],
            gender: [
                { id: "female", name: "female" },
                { id: "male", name: "male" },
            ],
            yesno: [
                { id: "yes", name: "yes" },
                { id: "no", name: "no" },
            ],
            questionnaire_id: ""
        }
        this.listQuestions = this.listQuestions.bind(this);
        this.getMyLocation = this.getMyLocation.bind(this)

    }

    componentWillMount() {
        this.getMyLocation();

        axios
            .get(`http://192.168.60.24:8000/api/survey/`)
            .then(res => {
                let options = res.data.map(item => {
                    return { id: item.id, name: item.survey_description };
                });
                console.log("Options created -", { options });
                this.setState({ questionnaire: options });
            })
            .catch(function (error) {
                console.log(error);
            });
    }
    getMyLocation() {
        const location = window.navigator && window.navigator.geolocation

        if (location) {
            location.getCurrentPosition((position) => {
                this.setState({
                    lattitude: position.coords.latitude,
                    longitude: position.coords.longitude,
                })
            }, (error) => {
                this.setState({ lattitude: 'err-latitude', longitude: 'err-longitude', load: false })
            })
        }

    }


    getQuestions(survey_id) {
        var container = document.getElementById("questionDiv");
        let atr = { id: "question", title: "question", name: "question", value: this.state.question, placeholder: "Enter the question", handleChange: this.handleChange },
            gender_list = { id: "question", title: "question", name: "question", options: this.state.gender, value: this.state.selectedTeam, placeholder: "Select Type of gender", handleChange: this.handleChange },
            yesno = { id: "question", title: "question", name: "question", options: this.state.yesno, value: this.state.selectedTeam, placeholder: "Select Type of ..", handleChange: this.handleChange }


        axios
            .get("http://192.168.60.24:8000/api/question/", { "survey_id": survey_id })
            .then(res => {
                res.data.map(ques => {
                    switch (ques.type) {
                        case "textfield":
                            var labelfield = document.createElement("Label");
                            labelfield.innerHTML = ques.question
                            var input = document.createElement("input");
                            input.type = "text";
                            input.name = "feedback"
                            input.id = "feedback"
                            input.value = []
                            input.handleChange = this.handleChange
                            input.placeholder = ques.question
                            input.className = "form-control";
                            var inputQuestion = document.createElement("input");
                            inputQuestion.type = "hidden";
                            inputQuestion.name = "question"
                            inputQuestion.id = "question"
                            inputQuestion.value = ques.id
                            inputQuestion.handleChange = this.handleChange
                            container.appendChild(inputQuestion);
                            container.appendChild(labelfield);
                            container.appendChild(input);
                            break;
                        case "datefield":
                            var labelfield = document.createElement("Label");
                            labelfield.innerHTML = ques.question
                            var input = document.createElement("input");
                            input.type = "text";
                            input.name = "feedback"
                            input.id = "feedback"
                            input.value = []
                            input.placeholder = ques.question
                            input.className = "form-control";
                            input.handleChange = this.handleChange
                            var inputQuestion = document.createElement("input");
                            inputQuestion.type = "hidden";
                            inputQuestion.name = "question"
                            inputQuestion.id = "question"
                            inputQuestion.value = ques.id
                            container.appendChild(inputQuestion);
                            container.appendChild(labelfield);
                            container.appendChild(input);
                            break;
                        case "genderfield":
                            var labelfield = document.createElement("Label");
                            labelfield.innerHTML = ques.question
                            var sel = document.createElement("select");
                            sel.name = 'feedback';
                            sel.id = 'feedback';
                            sel.className = "form-control";
                            var options_str = "";

                            this.state.gender.forEach(function (gen) {
                                options_str += '<option value="' + gen.id + '">' + gen.name + '</option>';
                            });
                            sel.innerHTML = options_str;
                            var inputQuestion = document.createElement("input");
                            inputQuestion.type = "hidden";
                            inputQuestion.name = "question"
                            inputQuestion.id = "question"
                            inputQuestion.value = ques.id
                            container.appendChild(inputQuestion);
                            container.appendChild(labelfield);
                            container.appendChild(sel);
                            break;
                        case "integer":
                            this.state.switchResult = (<input {...atr} inputType="text" />)
                            break;
                        case "yes/no":
                            this.state.switchResult = (<Select {...yesno} />)
                            break;
                        default:
                            this.state.switchResult = (<input {...atr} inputType="text" />)
                    }


                });
            })
            .catch(function (error) {
                console.log(error);
            });
    }

    listQuestions(e) {
        this.setState({
            [e.target.name]: e.target.value
        });

        this.getQuestions(this.state.questionnaire_id)

    }

    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log('New state -', {
            [e.target.name]: e.target.value
        })

    }

    handleFormSubmit = e => {
        e.preventDefault();
        const { longitude, lattitude, feedback, questionnaire_id } = this.state;
        console.log(this.state, "statehere")
        axios
            .post("http://192.168.60.24:8000/api/answer/", { longitude, lattitude, feedback, questionnaire_id })
            .then(err => {
                //access the err here....
            });
    };

    render() {
        const { lattitude, longitude } = this.state
        let lat_attr = { id: "lattitude", inputType: "text", title: "lattitude", name: "lattitude", value: this.state.lattitude, placeholder: "Enter the question", handleChange: this.handleChange },
            long_attr = { id: "longitude", inputType: "text", title: "longitude", name: "longitude", value: this.state.longitude, placeholder: "Enter the question", handleChange: this.handleChange },
            btn_attr = { action: this.handleFormSubmit, type: "primary", title: "Submit", style: buttonStyle },
            btn_clear = { action: this.handleClearForm, type: "secondary", title: "Clear", style: buttonStyle, }

        return (
            < div >
                {this.state.load == true ? (
                    <form className="container-fluid" onSubmit={this.handleFormSubmit} method="post" >
                        <div className="form-group">
                            <label for=""> Questionnaire </label>
                            <select className="form-control" onChange={this.listQuestions} name="questionnaire_id">
                                {this.state.questionnaire.map(item => {
                                    return <option value={item.id}>{item.name}</option>;
                                })}
                            </select>
                        </div>
                        <div className="form-group">
                            <label for="" className="form-label">
                                Questions loading..................
                            </label>
                            <div id="questionDiv"></div>
                        </div>

                        <Input {...long_attr} />
                        <Input {...lat_attr} />
                        <Button {...btn_attr} />
                        <Button {...btn_clear} />
                    </form>


                ) : (
                        <div><p>Location Has to be Set</p>
                            <button onClick={this.getMyLocation}>GET MY LOCATION</button></div>)}


            </div >
        )
    }
}
const buttonStyle = {
    margin: "10px 10px 10px 10px"
};
export default Feedback;