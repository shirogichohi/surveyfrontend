import React, { Component } from "react";

import Table from "react-bootstrap/Table";

class Tablemain extends Component {
  renderTableData() {
    return this.props.tablename.map((table, indext) => {
      return (
        <tr>
          {Object.keys(table).map((item, index) => {
            return <td>{table[item]}</td>;
          })}
        </tr>
      );
    });
  }

  renderTableHeader() {
    let header = Object.keys(this.props.tablename[0]);
    return header.map((key, index) => {
      return <th key={index}>{key.toUpperCase()}</th>;
    });
  }

  render() {
    console.log("Props passed", this.props);
    return (
      <div>
        <h1 id="title">{this.props.pageTitle}</h1>

        <Table striped bordered hover size="sm">
          <thead>
            <tr>{this.renderTableHeader()}</tr>
          </thead>
          <tbody>{this.renderTableData()}</tbody>
        </Table>
      </div>
    );
  }
}
export default Tablemain;
