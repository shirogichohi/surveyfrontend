import React, { Component } from "react";
import ReactDOM from "react-dom";
import { Modal, ModalManager, Effect } from "react-dynamic-modal";

class MyModal extends Component {
  render() {
    const { text, onRequestClose } = this.props;
    return (
      <Modal
        onRequestClose={onRequestClose}
        effect={Effect.ScaleUp}
        renderBackdrop={this.renderBackdrop}
        aria-labelledby="modal-label"
        className="Modal"
        overlayClassName="Overlay"
        style={{ top: 0 }}
      >
        <div className="modal-header">
          <h4 className="modal-title">New Record</h4>
        </div>
        <div class="modal-body">{text}</div>

        <div class="modal-footer">
          <button
            type="button"
            className="btn btn-default"
            onClick={ModalManager.close}
          >
            Close
          </button>
        </div>
      </Modal>
    );
  }
}

export default MyModal;
