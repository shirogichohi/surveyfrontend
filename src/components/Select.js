import React, { Component } from "react";
class Select extends Component {
  state = {};

  render() {
    console.log("hapa pa props", this.props);
    return (
      <div className="form-group ">
        <label className="col-sm-2 control-label ">Description</label>
        <div className="col-sm-10">
          <select>
            {this.props.items.map(item => {
              return <option value={item.key}>{item.value}</option>;
            })}
          </select>
        </div>
      </div>
    );
  }
}

export default Select;
