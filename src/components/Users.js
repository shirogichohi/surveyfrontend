import React, { Component } from "react";
import axios from "axios";
import { Modal, ModalManager, Effect } from "react-dynamic-modal";
import Staff from "../UsersPage/StaffPage";
import Board from "../UsersPage/BoardPage";
import Officer from "../UsersPage/OfficerPage";
import AdminUsers from "../views/Users/Admin";
import MyModal from "../components/Modal";


class Users extends Component {

  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      role: "",
      firstname: "",
      lastname: "",
      show: false,
      setShow: false,
      message: 'Please wait...'

    };
  }


  handleClose = () => this.state.setShow(false);
  handleShow = () => this.state.setShow(true);
  handleSubmit = e => {
    e.preventDefault();
    const { email, password, role, firstname, lastname } = this.state;
    axios
      .post("http://192.168.60.24:8000/api/users/", {
        email,
        password,
        role,
        firstname,
        lastname
      },
        function (data) { this.setState({ message: data }) })
      .then(result => {
        //access the results here....
        this.setState({ message: result })
      });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  openModal() {
    const text = (
      <div>
        <div className="result">{this.state.message}</div>
        <form
          method="POST"
          encType="multipart/form-data"
          className="form-horizontal"
          onSubmit={this.handleSubmit}
        >
          <fieldset>
            <div className="form-group ">
              <label className="col-sm-4 control-label ">Email address</label>
              <div className="col-sm-10">
                <input
                  name="email"
                  className="form-control"
                  type="email"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-group ">
              <label className="col-sm-2 control-label ">Password</label>
              <div className="col-sm-10">
                <input
                  name="password"
                  className="form-control"
                  type="password"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-group">
              <label className="col-sm-2 control-label ">Role</label>
              <div className="col-sm-10">
                <select
                  className="form-control"
                  name="role"
                  onChange={this.handleChange}
                  required
                >
                  <option value="">--Select role--</option>
                  <option value="staff">staff</option>
                  <option value="board">board</option>
                  <option value="officer">officer</option>
                </select>
              </div>
            </div>
            <div className="form-group ">
              <label className="col-sm-2 control-label ">Firstname</label>
              <div className="col-sm-10">
                <input
                  name="firstname"
                  className="form-control"
                  type="text"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-group ">
              <label className="col-sm-2 control-label ">Lastname</label>
              <div className="col-sm-10">
                <input
                  name="lastname"
                  className="form-control"
                  type="text"
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-actions">
              <button
                className="btn btn-primary js-tooltip"
                title="Make a POST request on the User Staff Viewset List resource"
              >
                POST
              </button>
            </div>
          </fieldset>
        </form>
      </div>
    );

    ModalManager.open(<MyModal text={text} onRequestClose={() => true} />);
  }

  render() {
    return (
      <div>

        <div className="mb-3 card card-body col-sm-2">
          <button
            type="button"
            className="btn btn-info"
            onClick={this.openModal.bind(this)}
          >
            Add new User{" "}
          </button>{" "}
        </div>
        <div className="mb-3 card">
          <div className="card-body">
            <ul className="tabs-animated-shadow tabs-animated nav">
              <li className="nav-item">
                <a
                  role="tab"
                  className="nav-link active"
                  id="tab-c-0"
                  data-toggle="tab"
                  href="#tab-animated-0"
                >
                  <span>Board Members</span>
                </a>
              </li>
              <li className="nav-item">
                <a
                  role="tab"
                  className="nav-link"
                  id="tab-c-1"
                  data-toggle="tab"
                  href="#tab-animated-1"
                >
                  <span>Staff on Ground</span>
                </a>
              </li>
              <li className="nav-item">
                <a
                  role="tab"
                  className="nav-link"
                  id="tab-c-2"
                  data-toggle="tab"
                  href="#tab-animated-2"
                >
                  <span>Regional Officers</span>
                </a>
              </li>
              <li className="nav-item">
                <a
                  role="tab"
                  className="nav-link"
                  id="tab-c-2"
                  data-toggle="tab"
                  href="#tab-animated-3"
                >
                  <span>Admin</span>
                </a>
              </li>
            </ul>
            <div className="tab-content">
              <div
                className="tab-pane active"
                id="tab-animated-0"
                role="tabpanel"
              >
                <p className="mb-0">
                  <h2>Board Members</h2>
                  <Board />
                </p>
              </div>
              <div className="tab-pane" id="tab-animated-1" role="tabpanel">
                <p className="mb-0">
                  <h2>Staff on Ground </h2>
                  <Staff />
                </p>
              </div>
              <div className="tab-pane" id="tab-animated-2" role="tabpanel">
                <p className="mb-0">
                  <h2>Regional Officer</h2>
                  <Officer />
                </p>
              </div>
              <div className="tab-pane" id="tab-animated-3" role="tabpanel">
                <p className="mb-0">
                  <h2>Admin</h2>
                  <AdminUsers />
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Users;
