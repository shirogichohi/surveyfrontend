import { authHeader } from "../_helpers";

export const userService = {
  login,
  logout,
  getAll
};

function login(email, password) {
  logout();
  const requestOptions = {
    method: "POST",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({ password, email })
  };

  return fetch(`http://192.168.60.24:8000/rest-auth/login/`, requestOptions)
    .then(handleResponse)
    .then(user => {
      // login successful if there's a jwt token in the response
      if (user.key) {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        localStorage.setItem("user", JSON.stringify(user));
        const userDetails = {
          email: user.user.email,
          firstname: user.user.lastname,
          role: user.user.role
        };
        localStorage.setItem("userDetails", JSON.stringify(userDetails));
        localStorage.setItem("userDetails", JSON.stringify(userDetails));
      }
      console.log("users", user);
      return user;
    });
}

function logout() {
  // remove user from local storage to log user out
  localStorage.removeItem("user");
  localStorage.removeItem("userDetails");
  localStorage.removeItem("questionnaire_id");
}

function getAll() {
  const requestOptions = {
    method: "GET",
    headers: authHeader()
  };

  return fetch(`/users`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        // auto logout if 401 response returned from api
        logout();
        window.location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
