import React, { Component } from "react";
import Tablemain from "../components/Table";
import { connect } from "react-redux";
import axios from "axios";
import { authHeader, host } from "../_helpers/auth-header";

export default class Board extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  componentWillMount() {
    const requestOptions = {
      method: "GET",
      headers: authHeader()
    };
    axios({
      method: "get",
      url: host() + "api/board/",
      timeout: 4000,
      headers: authHeader()
    })
      .then(res => {
        this.setState({ tablename: res.data });
      })
      .catch(error => console.error("timeout exceeded"));
  }

  render() {
    return this.state.tablename && this.state.tablename.length ? (
      <Tablemain {...this.state} />
    ) : (
      <div>Loading the table.....</div>
    );
  }
}
