import React, { Component } from "react";
import Tablemain from "../components/Table";
import axios from "axios";
import { authHeader, host } from "../_helpers";

export default class Staff extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  componentWillMount() {
    axios({
      method: "get",
      url: host() + "api/staff/",
      timeout: 4000,
      headers: authHeader()
    })
      .then(res => {
        this.setState({ tablename: res.data });
      })
      .catch(error => console.error("timeout exceeded"));
  }

  render() {
    return this.state.tablename && this.state.tablename.length ? (
      <Tablemain {...this.state} />
    ) : (
      <div>Loading the table.....</div>
    );
  }
}
