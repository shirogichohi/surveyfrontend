import React, {Component} from "react";
import axios from "axios";
import Staff from "../UsersPage/StaffPage";
import Board from "../UsersPage/BoardPage";
import Officer from "../UsersPage/OfficerPage";
import AdminUsers from "../views/Users/Admin";
import {authHeader, host} from "../_helpers/auth-header";
import HandleError  from "../_helpers/error-handler";
import {history} from '../_helpers';

class Users extends Component {
    constructor(props) {
        super(props);
        this.state = {
            email: "",
            password: "",
            role: "",
            firstname: "",
            lastname: "",
            successMsg: "",
            errorMsg: ""
        };
    }

    handleSubmit = e => {
        e.preventDefault();
        const {email, password, role, firstname, lastname} = this.state;
        axios({
            method: "post",
            url: host() + "api/users/",
            timeout: 4000,
            headers: authHeader(),
            data: {email, password, role, firstname, lastname}
        })
            .then(res => {
                history.push('/users');
                this.setState({successMsg: "Record Added Successfully"});

            }, error => {
                console.log("apartment",error)
                let jsonner = error.response.data,
                    message = ''
                Object.keys(jsonner).map(item => {
                    message = message + item + ' : ' + jsonner[item] + ';\n'
                 })

                this.setState({errorMsg: [message]})
                console.log('INNER ERROR OCCURRED', error.response.data)
               // alert('Inner error occurred \n' + message)
            })
            .catch(error => {
                    console.log('OUTER ERROR OCCURRED')
                    this.setState({errorMsg: "Failed . Try Again"})
                    console.log(error.response.data)
                }
            )

    };

    handleChange = e => {
        this.setState({[e.target.name]: e.target.value});
    };
   render() {
        return (
            <div className="mb-3 card">
                <div className="card-header">
                    Add New User
                </div>
                <form
                    method="POST"
                    encType="multipart/form-data"
                    className="form-horizontal"
                    onSubmit={this.handleSubmit}
                    id='form'
                >
                    <fieldset>

                        <div className="row">
                            <div className="col-sm-4">
                                <div className="form-group ">
                                    <label className="col-sm-6 control-label ">Firstname</label>
                                    <div className="col-sm-10">
                                        <input
                                            name="firstname"
                                            className="form-control"
                                            type="text"
                                            onChange={this.handleChange}
                                        />
                                    </div>
                                </div>
                                <div className="form-group ">
                                    <label className="col-sm-6 control-label ">Lastname</label>
                                    <div className="col-sm-10">
                                        <input
                                            name="lastname"
                                            className="form-control"
                                            type="text"
                                            onChange={this.handleChange}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="form-group">
                                    <label className="col-sm-6 control-label ">Role</label>
                                    <div className="col-sm-10">
                                        <select
                                            className="form-control"
                                            name="role"
                                            onChange={this.handleChange}
                                            required
                                        >
                                            <option value="">--Select role--</option>
                                            <option value="staff">staff</option>
                                            <option value="board">board</option>
                                            <option value="officer">officer</option>
                                            <option value="officer">admin</option>
                                        </select>
                                    </div>
                                </div>
                                <div className="form-group ">
                                    <label className="col-sm-6 control-label ">Password</label>
                                    <div className="col-sm-10">
                                        <input
                                            name="password"
                                            className="form-control"
                                            type="password"
                                            onChange={this.handleChange}
                                        />
                                    </div>
                                </div>
                            </div>
                            <div className="col-sm-4">
                                <div className="form-group ">
                                    <label className="col-sm-4 control-label ">
                                        Email address
                                    </label>
                                    <div className="col-sm-10">
                                        <input
                                            name="email"
                                            className="form-control"
                                            type="email"
                                            onChange={this.handleChange}
                                        />
                                    </div>
                                </div>
                                <div className="form-group ">
                                    <div className="col-sm-10">
                                        <button
                                            className="btn btn-primary js-tooltip"
                                            title="Make a POST request on the User Staff Viewset List resource"
                                        >
                                            SAVE
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                </form>
                <HandleError {...this.state}/>
                <div className="card">
                    <div className="card-header">
                        List of Users
                    </div>
                    <div className="card-body">
                        <ul className="tabs-animated-shadow tabs-animated nav">
                            <li className="nav-item">
                                <a
                                    role="tab"
                                    className="nav-link active"
                                    id="tab-c-0"
                                    data-toggle="tab"
                                    href="#tab-animated-0"
                                >
                                    <span>Board Members</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    role="tab"
                                    className="nav-link"
                                    id="tab-c-1"
                                    data-toggle="tab"
                                    href="#tab-animated-1"
                                >
                                    <span>Staff on Ground</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    role="tab"
                                    className="nav-link"
                                    id="tab-c-2"
                                    data-toggle="tab"
                                    href="#tab-animated-2"
                                >
                                    <span>Regional Officers</span>
                                </a>
                            </li>
                            <li className="nav-item">
                                <a
                                    role="tab"
                                    className="nav-link"
                                    id="tab-c-2"
                                    data-toggle="tab"
                                    href="#tab-animated-3"
                                >
                                    <span>Admin</span>
                                </a>
                            </li>
                        </ul>
                        <div className="tab-content">
                            <div
                                className="tab-pane active"
                                id="tab-animated-0"
                                role="tabpanel"
                            >
                                <p className="mb-0">
                                    <h2>Board Members</h2>
                                    <Board/>
                                </p>
                            </div>
                            <div className="tab-pane" id="tab-animated-1" role="tabpanel">
                                <p className="mb-0">
                                    <h2>Staff on Ground </h2>
                                    <Staff/>
                                </p>
                            </div>
                            <div className="tab-pane" id="tab-animated-2" role="tabpanel">
                                <p className="mb-0">
                                    <h2>Regional Officer</h2>
                                    <Officer/>
                                </p>
                            </div>
                            <div className="tab-pane" id="tab-animated-3" role="tabpanel">
                                <p className="mb-0">
                                    <h2>Admin</h2>
                                    <AdminUsers/>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Users;
