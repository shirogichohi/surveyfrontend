import React, { Component } from "react";
import axios from "axios";

/* Import Components */
import {Map, TileLayer, Marker, Popup, CircleMarker, withLeaflet} from "react-leaflet";
import "leaflet/dist/leaflet.css";
import L from "leaflet";
import { ReactLeafletSearch } from "react-leaflet-search";
import { authHeader, host } from "../_helpers/auth-header";
import data from "./Cities";
class MapHome extends Component {
  constructor(props) {
    super(props);
    this.state = {
      questionnaire: [],
      cities: []
    };
    axios({
      method: "get",
      url: host() + "api/answer/",
      timeout: 4000,
      headers: authHeader()
    }).then(result => {
      this.setState({ maps: result.data });
      let maps = result.data.map(item => {
        return {
          name: "Nairobi",
          coordinates: [item.lattitude, item.longitude],
          population: 200
        };
      });
      this.setState({
        ...this.state,
        cities: maps
      });
    });
  }

  render() {
    delete L.Icon.Default.prototype._getIconUrl;
    const ReactLeafletSearchComponent = withLeaflet(ReactLeafletSearch);

    L.Icon.Default.mergeOptions({
      iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png"),
      iconUrl: require("leaflet/dist/images/marker-icon.png"),
      shadowUrl: require("leaflet/dist/images/marker-shadow.png")
    });
    let ques_options = this.state.questionnaire.map(item => {
      return <option value={item.id}>{item.name}</option>;
    });

    return (
      <div>
        <Map
          style={{ height: "1040px", width: "100%", margin: "2px" }}
          zoom={7}
          center={[0.0236, 37.9062]}
           className="simpleMap"
           scrollWheelZoom={true}
        bounds={this.state.bounds}
        maxZoom={this.state.maxZoom}
        maxBounds={this.state.maxBounds}
        >
          <TileLayer url="http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"  noWrap={true} />
<ReactLeafletSearchComponent
          customProvider={this.provider}
          position="topleft"
          search={[]}
          inputPlaceholder="Search Region"
          showMarker={true}
          zoom={7}
          showPopup={true}
         searchBounds={[
            [0.0236, 37.9062],
            [8.7832,34.5085]
          ]}
          popUp={this.customPopup}
          closeResultsOnClick={true}
          openSearchOnLoad={true}
       />
          {this.state.cities
            ? this.state.cities.map(city => {
                return (
                  <Marker
                    position={[city["coordinates"][0], city["coordinates"][1]]}
                  >
                    <Popup>
                      Total at {city.name} : {city.population}
                    </Popup>
                  </Marker>
                );
              })
            : data.city.map(city => {
                return (
                  <Marker
                    position={[city["coordinates"][0], city["coordinates"][1]]}
                  >
                    <Popup>
                      Total at {city.name} : {city.population}
                    </Popup>
                  </Marker>
                );
              })}
          }
        </Map>
      </div>
    );
  }
}

export default MapHome;
