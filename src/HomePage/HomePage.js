import React from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import axios from "axios";
import { authHeader } from "../_helpers/auth-header";
import Select from "../components/formelements//Select";
class HomePage extends React.Component {
  state = {
    questionnaire: [],
    survey_id: ""
  };
  componentWillMount() {
    const requestOptions = {
      method: "GET",
      headers: authHeader()
    };
    axios
      .get(`http://192.168.60.24:8000/api/survey`, requestOptions)
      .then(res => {
        let options = res.data.map(item => {
          return { id: item.id, name: item.survey_description };
        });
        console.log("Options created -", { options });
        this.setState({ questionnaire: options });
      })
      .catch(function(error) {
        console.log(error);
      });
  }
  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };
  handleFormSubmit = e => {
    e.preventDefault();
    localStorage.removeItem("questionnaire_id");
    const { survey_id } = this.state;
    localStorage.setItem("questionnaire_id", JSON.stringify(survey_id));
    this.props.history.push("/map");
  };
  render() {
    const { user } = this.props;
    let survey_attr = {
      id: "survey_id",
      title: "Questionnaire",
      name: "survey_id",
      options: this.state.questionnaire,
      value: this.state.survey_id,
      placeholder: "Select Questionnaire",
      handleChange: this.handleChange
    };

    return (
      <div>
        <div className="col-md-6 col-md-offset-3">
          <h1>
            Hi {user.user.firstname} {user.user.lastname}!
          </h1>
          <p>Please choose the questionnaire to preoceed</p>

          <form
            className="container-fluid"
            onSubmit={this.handleFormSubmit}
            method="post"
          >
            <Select {...survey_attr} />

            <div className="form-group">
              <button
                className="btn btn-primary js-tooltip"
                title="Make a POST request on the User Staff Viewset List resource"
              >
                POST
              </button>
            </div>
          </form>
          <p>
            <Link to="/login">Logout</Link>
          </p>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { authentication } = state;
  const { user } = authentication;
  return {
    user
  };
}

const connectedHomePage = connect(mapStateToProps)(HomePage);
export { connectedHomePage as HomePage };
