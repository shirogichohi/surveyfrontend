import React, { Component } from "react";
class ProfilePage extends Component {
  state = {};
  render() {
    const userDetils = JSON.parse(localStorage.getItem("userDetails"));
    return (
      <div>
        <div className="container-fluid user-profile">
          <div className="row">
            <div className="col-md-12">
              <div className="well well-sm">
                <div className="user-profile-card">
                  <div className="user-profile-header"></div>
                  <div className="user-profile-avatar text-center">
                    <img alt="" src="assets/images/avatars/1.jpg" />
                  </div>
                  <h3>
                    <b>Account Details</b>{" "}
                  </h3>
                  <div className="col-sm-12 col-sm-8 text-cemter">
                    <div className=" jumbotron  row">
                      <div className="col-md-6">
                        <p>
                          <small>Email</small>
                          {userDetils.email ? userDetils.email : "Not added"}
                        </p>
                        <p>
                          <small>Logged in as : </small>
                          {userDetils.role ? userDetils.role : "Not Added"}
                        </p>
                      </div>
                      <div className="col-md-6">
                        <p>
                          <small>Firstname</small>
                          {userDetils.firstname
                            ? userDetils.firstname
                            : "Not Added"}
                        </p>
                        <p>
                          <small>Lastname</small>
                          {userDetils.lastname
                            ? userDetils.lastname
                            : "Not Added"}
                        </p>
                      </div>
                    </div>
                  </div>
                  <div className="panel-footer">
                    <a
                      href="./my_account_edit"
                      data-original-title="Edit My Profile"
                      data-toggle="tooltip"
                      type="button"
                      className="btn btn-sm btn-primary"
                    >
                      <i className="glyphicon glyphicon-edit" /> Edit My Profile
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default ProfilePage;
