import React, { Component } from "react";
import axios from "axios";
import { authHeader, host } from "../_helpers";
class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      new_password1: "",
      new_password2: ""
    };
  }

  handleSubmit = e => {
    e.preventDefault();
    const requestOptions = {
      method: "POST",
      headers: authHeader()
    };
    axios({
      method: "post",
      url: host() + "rest-auth/password/change/",
      timeout: 4000,
      headers: authHeader(),
      data: { ...this.state }
    })
      .then(result => {})
      .catch(err => {
        let message = ({ err } = this.state);
      });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    return (
      <div className="col-sm-6">
        <form
          method="POST"
          encType="multipart/form-data"
          className="form-horizontal"
          onSubmit={this.handleSubmit}
        >
          <fieldset>
            <div className="form-group ">
              <label className="col-sm-2 control-label ">Password</label>
              <div className="col-sm-10">
                <input
                  type="hidden"
                  name="csrfmiddlewaretoken"
                  value="9v7SQH5Ql65I0eoBmvJVM1QUCmnzYID9sCRRJdueX2tcgXjGHztGLbpZmo92NGFk"
                />
                <input
                  name="new_password1"
                  id="new_password1"
                  className="form-control"
                  type="password"
                  required
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-group ">
              <label className="col-sm-4 control-label ">
                Confirm Password
              </label>
              <div className="col-sm-10">
                <input
                  name="new_password2"
                  id="new_password2"
                  className="form-control"
                  type="password"
                  required
                  onChange={this.handleChange}
                />
              </div>
            </div>
            <div className="form-group ">
              <div className="col-sm-10">
                <button className="btn btn-primary js-tooltip">
                  Change Password
                </button>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    );
  }
}

export default ChangePassword;
